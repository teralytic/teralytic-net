VERSION := `npm view @modelrocket/mvault version`
GITLAB_PROJECT := 8558474
SWAGGER_CODEGEN ?= $(shell which swagger-codegen)
SWAGGER_SPEC ?= swagger.yaml

default: publish

fetch:
	curl -o swagger.yaml --header 'PRIVATE-TOKEN: $(GITLAB_TOKEN)' https://gitlab.com/api/v4/projects/$(GITLAB_PROJECT)/repository/files/api%2Fswagger.yaml/raw?ref=master

client: fetch
	$(SWAGGER_CODEGEN) generate -l csharp -i $(SWAGGER_SPEC) -c config.json -o ./client

.PHONY: \
	client
