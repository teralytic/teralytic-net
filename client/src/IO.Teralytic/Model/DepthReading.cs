/* 
 * teralytic
 *
 * Teralytic-API allows customers and partners to view their probes, sensor readings for each, as well as analytics and calculations from Teralytic algorithms.
 *
 * OpenAPI spec version: 1.0.5
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = IO.Teralytic.Client.SwaggerDateConverter;

namespace IO.Teralytic.Model
{
    /// <summary>
    /// DepthReading
    /// </summary>
    [DataContract]
    public partial class DepthReading :  IEquatable<DepthReading>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DepthReading" /> class.
        /// </summary>
        /// <param name="Depth">The depth value.</param>
        /// <param name="DepthUnits">The depth units (i.e. in, cm).</param>
        /// <param name="Terascore">TeraScore.</param>
        /// <param name="Nitrogen">Nitrogen.</param>
        /// <param name="NitrogenPpm">Nitrogen ppm.</param>
        /// <param name="Phosphorus">Phosphorus.</param>
        /// <param name="PhosphorusPpm">Phosphorus ppm.</param>
        /// <param name="Potassium">Potassium.</param>
        /// <param name="PotassiumPpm">Potassium ppm.</param>
        /// <param name="Ec">Electrical Conductivity.</param>
        /// <param name="O2">Dioxygen.</param>
        /// <param name="PH">pH Scale.</param>
        /// <param name="Co2">Carbon Dioxide.</param>
        /// <param name="Awc">Awc.</param>
        /// <param name="SoilMoisture">Soil Moisture.</param>
        /// <param name="SoilTemperature">Soil Temperature.</param>
        /// <param name="SoilTexture">Soil texture type. Data are collected from government data sources (USDA&#39;s SSURGO database for fields in the United States).</param>
        /// <param name="ExtendedAttributes">Additional properties.</param>
        public DepthReading(long? Depth = default(long?), string DepthUnits = default(string), decimal? Terascore = default(decimal?), decimal? Nitrogen = default(decimal?), decimal? NitrogenPpm = default(decimal?), decimal? Phosphorus = default(decimal?), decimal? PhosphorusPpm = default(decimal?), decimal? Potassium = default(decimal?), decimal? PotassiumPpm = default(decimal?), decimal? Ec = default(decimal?), decimal? O2 = default(decimal?), decimal? PH = default(decimal?), decimal? Co2 = default(decimal?), decimal? Awc = default(decimal?), decimal? SoilMoisture = default(decimal?), decimal? SoilTemperature = default(decimal?), string SoilTexture = default(string), Dictionary<string, Object> ExtendedAttributes = default(Dictionary<string, Object>))
        {
            this.Depth = Depth;
            this.DepthUnits = DepthUnits;
            this.Terascore = Terascore;
            this.Nitrogen = Nitrogen;
            this.NitrogenPpm = NitrogenPpm;
            this.Phosphorus = Phosphorus;
            this.PhosphorusPpm = PhosphorusPpm;
            this.Potassium = Potassium;
            this.PotassiumPpm = PotassiumPpm;
            this.Ec = Ec;
            this.O2 = O2;
            this.PH = PH;
            this.Co2 = Co2;
            this.Awc = Awc;
            this.SoilMoisture = SoilMoisture;
            this.SoilTemperature = SoilTemperature;
            this.SoilTexture = SoilTexture;
            this.ExtendedAttributes = ExtendedAttributes;
        }
        
        /// <summary>
        /// The depth value
        /// </summary>
        /// <value>The depth value</value>
        [DataMember(Name="depth", EmitDefaultValue=false)]
        public long? Depth { get; set; }

        /// <summary>
        /// The depth units (i.e. in, cm)
        /// </summary>
        /// <value>The depth units (i.e. in, cm)</value>
        [DataMember(Name="depth_units", EmitDefaultValue=false)]
        public string DepthUnits { get; set; }

        /// <summary>
        /// TeraScore
        /// </summary>
        /// <value>TeraScore</value>
        [DataMember(Name="terascore", EmitDefaultValue=false)]
        public decimal? Terascore { get; set; }

        /// <summary>
        /// Nitrogen
        /// </summary>
        /// <value>Nitrogen</value>
        [DataMember(Name="nitrogen", EmitDefaultValue=false)]
        public decimal? Nitrogen { get; set; }

        /// <summary>
        /// Nitrogen ppm
        /// </summary>
        /// <value>Nitrogen ppm</value>
        [DataMember(Name="nitrogen_ppm", EmitDefaultValue=false)]
        public decimal? NitrogenPpm { get; set; }

        /// <summary>
        /// Phosphorus
        /// </summary>
        /// <value>Phosphorus</value>
        [DataMember(Name="phosphorus", EmitDefaultValue=false)]
        public decimal? Phosphorus { get; set; }

        /// <summary>
        /// Phosphorus ppm
        /// </summary>
        /// <value>Phosphorus ppm</value>
        [DataMember(Name="phosphorus_ppm", EmitDefaultValue=false)]
        public decimal? PhosphorusPpm { get; set; }

        /// <summary>
        /// Potassium
        /// </summary>
        /// <value>Potassium</value>
        [DataMember(Name="potassium", EmitDefaultValue=false)]
        public decimal? Potassium { get; set; }

        /// <summary>
        /// Potassium ppm
        /// </summary>
        /// <value>Potassium ppm</value>
        [DataMember(Name="potassium_ppm", EmitDefaultValue=false)]
        public decimal? PotassiumPpm { get; set; }

        /// <summary>
        /// Electrical Conductivity
        /// </summary>
        /// <value>Electrical Conductivity</value>
        [DataMember(Name="ec", EmitDefaultValue=false)]
        public decimal? Ec { get; set; }

        /// <summary>
        /// Dioxygen
        /// </summary>
        /// <value>Dioxygen</value>
        [DataMember(Name="o2", EmitDefaultValue=false)]
        public decimal? O2 { get; set; }

        /// <summary>
        /// pH Scale
        /// </summary>
        /// <value>pH Scale</value>
        [DataMember(Name="pH", EmitDefaultValue=false)]
        public decimal? PH { get; set; }

        /// <summary>
        /// Carbon Dioxide
        /// </summary>
        /// <value>Carbon Dioxide</value>
        [DataMember(Name="co2", EmitDefaultValue=false)]
        public decimal? Co2 { get; set; }

        /// <summary>
        /// Gets or Sets Awc
        /// </summary>
        [DataMember(Name="awc", EmitDefaultValue=false)]
        public decimal? Awc { get; set; }

        /// <summary>
        /// Soil Moisture
        /// </summary>
        /// <value>Soil Moisture</value>
        [DataMember(Name="soil_moisture", EmitDefaultValue=false)]
        public decimal? SoilMoisture { get; set; }

        /// <summary>
        /// Soil Temperature
        /// </summary>
        /// <value>Soil Temperature</value>
        [DataMember(Name="soil_temperature", EmitDefaultValue=false)]
        public decimal? SoilTemperature { get; set; }

        /// <summary>
        /// Soil texture type. Data are collected from government data sources (USDA&#39;s SSURGO database for fields in the United States)
        /// </summary>
        /// <value>Soil texture type. Data are collected from government data sources (USDA&#39;s SSURGO database for fields in the United States)</value>
        [DataMember(Name="soil_texture", EmitDefaultValue=false)]
        public string SoilTexture { get; set; }

        /// <summary>
        /// Additional properties
        /// </summary>
        /// <value>Additional properties</value>
        [DataMember(Name="extendedAttributes", EmitDefaultValue=false)]
        public Dictionary<string, Object> ExtendedAttributes { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class DepthReading {\n");
            sb.Append("  Depth: ").Append(Depth).Append("\n");
            sb.Append("  DepthUnits: ").Append(DepthUnits).Append("\n");
            sb.Append("  Terascore: ").Append(Terascore).Append("\n");
            sb.Append("  Nitrogen: ").Append(Nitrogen).Append("\n");
            sb.Append("  NitrogenPpm: ").Append(NitrogenPpm).Append("\n");
            sb.Append("  Phosphorus: ").Append(Phosphorus).Append("\n");
            sb.Append("  PhosphorusPpm: ").Append(PhosphorusPpm).Append("\n");
            sb.Append("  Potassium: ").Append(Potassium).Append("\n");
            sb.Append("  PotassiumPpm: ").Append(PotassiumPpm).Append("\n");
            sb.Append("  Ec: ").Append(Ec).Append("\n");
            sb.Append("  O2: ").Append(O2).Append("\n");
            sb.Append("  PH: ").Append(PH).Append("\n");
            sb.Append("  Co2: ").Append(Co2).Append("\n");
            sb.Append("  Awc: ").Append(Awc).Append("\n");
            sb.Append("  SoilMoisture: ").Append(SoilMoisture).Append("\n");
            sb.Append("  SoilTemperature: ").Append(SoilTemperature).Append("\n");
            sb.Append("  SoilTexture: ").Append(SoilTexture).Append("\n");
            sb.Append("  ExtendedAttributes: ").Append(ExtendedAttributes).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as DepthReading);
        }

        /// <summary>
        /// Returns true if DepthReading instances are equal
        /// </summary>
        /// <param name="input">Instance of DepthReading to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(DepthReading input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.Depth == input.Depth ||
                    (this.Depth != null &&
                    this.Depth.Equals(input.Depth))
                ) && 
                (
                    this.DepthUnits == input.DepthUnits ||
                    (this.DepthUnits != null &&
                    this.DepthUnits.Equals(input.DepthUnits))
                ) && 
                (
                    this.Terascore == input.Terascore ||
                    (this.Terascore != null &&
                    this.Terascore.Equals(input.Terascore))
                ) && 
                (
                    this.Nitrogen == input.Nitrogen ||
                    (this.Nitrogen != null &&
                    this.Nitrogen.Equals(input.Nitrogen))
                ) && 
                (
                    this.NitrogenPpm == input.NitrogenPpm ||
                    (this.NitrogenPpm != null &&
                    this.NitrogenPpm.Equals(input.NitrogenPpm))
                ) && 
                (
                    this.Phosphorus == input.Phosphorus ||
                    (this.Phosphorus != null &&
                    this.Phosphorus.Equals(input.Phosphorus))
                ) && 
                (
                    this.PhosphorusPpm == input.PhosphorusPpm ||
                    (this.PhosphorusPpm != null &&
                    this.PhosphorusPpm.Equals(input.PhosphorusPpm))
                ) && 
                (
                    this.Potassium == input.Potassium ||
                    (this.Potassium != null &&
                    this.Potassium.Equals(input.Potassium))
                ) && 
                (
                    this.PotassiumPpm == input.PotassiumPpm ||
                    (this.PotassiumPpm != null &&
                    this.PotassiumPpm.Equals(input.PotassiumPpm))
                ) && 
                (
                    this.Ec == input.Ec ||
                    (this.Ec != null &&
                    this.Ec.Equals(input.Ec))
                ) && 
                (
                    this.O2 == input.O2 ||
                    (this.O2 != null &&
                    this.O2.Equals(input.O2))
                ) && 
                (
                    this.PH == input.PH ||
                    (this.PH != null &&
                    this.PH.Equals(input.PH))
                ) && 
                (
                    this.Co2 == input.Co2 ||
                    (this.Co2 != null &&
                    this.Co2.Equals(input.Co2))
                ) && 
                (
                    this.Awc == input.Awc ||
                    (this.Awc != null &&
                    this.Awc.Equals(input.Awc))
                ) && 
                (
                    this.SoilMoisture == input.SoilMoisture ||
                    (this.SoilMoisture != null &&
                    this.SoilMoisture.Equals(input.SoilMoisture))
                ) && 
                (
                    this.SoilTemperature == input.SoilTemperature ||
                    (this.SoilTemperature != null &&
                    this.SoilTemperature.Equals(input.SoilTemperature))
                ) && 
                (
                    this.SoilTexture == input.SoilTexture ||
                    (this.SoilTexture != null &&
                    this.SoilTexture.Equals(input.SoilTexture))
                ) && 
                (
                    this.ExtendedAttributes == input.ExtendedAttributes ||
                    this.ExtendedAttributes != null &&
                    this.ExtendedAttributes.SequenceEqual(input.ExtendedAttributes)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.Depth != null)
                    hashCode = hashCode * 59 + this.Depth.GetHashCode();
                if (this.DepthUnits != null)
                    hashCode = hashCode * 59 + this.DepthUnits.GetHashCode();
                if (this.Terascore != null)
                    hashCode = hashCode * 59 + this.Terascore.GetHashCode();
                if (this.Nitrogen != null)
                    hashCode = hashCode * 59 + this.Nitrogen.GetHashCode();
                if (this.NitrogenPpm != null)
                    hashCode = hashCode * 59 + this.NitrogenPpm.GetHashCode();
                if (this.Phosphorus != null)
                    hashCode = hashCode * 59 + this.Phosphorus.GetHashCode();
                if (this.PhosphorusPpm != null)
                    hashCode = hashCode * 59 + this.PhosphorusPpm.GetHashCode();
                if (this.Potassium != null)
                    hashCode = hashCode * 59 + this.Potassium.GetHashCode();
                if (this.PotassiumPpm != null)
                    hashCode = hashCode * 59 + this.PotassiumPpm.GetHashCode();
                if (this.Ec != null)
                    hashCode = hashCode * 59 + this.Ec.GetHashCode();
                if (this.O2 != null)
                    hashCode = hashCode * 59 + this.O2.GetHashCode();
                if (this.PH != null)
                    hashCode = hashCode * 59 + this.PH.GetHashCode();
                if (this.Co2 != null)
                    hashCode = hashCode * 59 + this.Co2.GetHashCode();
                if (this.Awc != null)
                    hashCode = hashCode * 59 + this.Awc.GetHashCode();
                if (this.SoilMoisture != null)
                    hashCode = hashCode * 59 + this.SoilMoisture.GetHashCode();
                if (this.SoilTemperature != null)
                    hashCode = hashCode * 59 + this.SoilTemperature.GetHashCode();
                if (this.SoilTexture != null)
                    hashCode = hashCode * 59 + this.SoilTexture.GetHashCode();
                if (this.ExtendedAttributes != null)
                    hashCode = hashCode * 59 + this.ExtendedAttributes.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
