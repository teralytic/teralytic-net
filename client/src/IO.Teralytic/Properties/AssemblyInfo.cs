using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Teralytic API Client for .NET")]
[assembly: AssemblyDescription("Teralytic Readings API")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Teralytic")]
[assembly: AssemblyProduct("Teralytic API")]
[assembly: AssemblyCopyright("Copyright 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.5")]
[assembly: AssemblyFileVersion("1.0.5")]
[assembly: Guid("4DD25357-A132-44A7-998B-F83B8B415962")]

