/* 
 * teralytic
 *
 * Teralytic-API allows customers and partners to view their probes, sensor readings for each, as well as analytics and calculations from Teralytic algorithms.
 *
 * OpenAPI spec version: 1.0.5
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Teralytic.Api;
using IO.Teralytic.Model;
using IO.Teralytic.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Teralytic.Test
{
    /// <summary>
    ///  Class for testing ReadingLocation
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class ReadingLocationTests
    {
        // TODO uncomment below to declare an instance variable for ReadingLocation
        //private ReadingLocation instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of ReadingLocation
            //instance = new ReadingLocation();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of ReadingLocation
        /// </summary>
        [Test]
        public void ReadingLocationInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" ReadingLocation
            //Assert.IsInstanceOfType<ReadingLocation> (instance, "variable 'instance' is a ReadingLocation");
        }


        /// <summary>
        /// Test the property 'Latitude'
        /// </summary>
        [Test]
        public void LatitudeTest()
        {
            // TODO unit test for the property 'Latitude'
        }
        /// <summary>
        /// Test the property 'Longitude'
        /// </summary>
        [Test]
        public void LongitudeTest()
        {
            // TODO unit test for the property 'Longitude'
        }
        /// <summary>
        /// Test the property 'Geohash'
        /// </summary>
        [Test]
        public void GeohashTest()
        {
            // TODO unit test for the property 'Geohash'
        }

    }

}
