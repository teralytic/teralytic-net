/* 
 * teralytic
 *
 * Teralytic-API allows customers and partners to view their probes, sensor readings for each, as well as analytics and calculations from Teralytic algorithms.
 *
 * OpenAPI spec version: 1.0.5
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Teralytic.Api;
using IO.Teralytic.Model;
using IO.Teralytic.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Teralytic.Test
{
    /// <summary>
    ///  Class for testing FieldGeometry
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class FieldGeometryTests
    {
        // TODO uncomment below to declare an instance variable for FieldGeometry
        //private FieldGeometry instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of FieldGeometry
            //instance = new FieldGeometry();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of FieldGeometry
        /// </summary>
        [Test]
        public void FieldGeometryInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" FieldGeometry
            //Assert.IsInstanceOfType<FieldGeometry> (instance, "variable 'instance' is a FieldGeometry");
        }


        /// <summary>
        /// Test the property 'Id'
        /// </summary>
        [Test]
        public void IdTest()
        {
            // TODO unit test for the property 'Id'
        }
        /// <summary>
        /// Test the property 'Type'
        /// </summary>
        [Test]
        public void TypeTest()
        {
            // TODO unit test for the property 'Type'
        }
        /// <summary>
        /// Test the property 'Geometry'
        /// </summary>
        [Test]
        public void GeometryTest()
        {
            // TODO unit test for the property 'Geometry'
        }

    }

}
