/* 
 * teralytic
 *
 * Teralytic-API allows customers and partners to view their probes, sensor readings for each, as well as analytics and calculations from Teralytic algorithms.
 *
 * OpenAPI spec version: 1.0.5
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Teralytic.Api;
using IO.Teralytic.Model;
using IO.Teralytic.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Teralytic.Test
{
    /// <summary>
    ///  Class for testing Reading
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class ReadingTests
    {
        // TODO uncomment below to declare an instance variable for Reading
        //private Reading instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of Reading
            //instance = new Reading();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of Reading
        /// </summary>
        [Test]
        public void ReadingInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" Reading
            //Assert.IsInstanceOfType<Reading> (instance, "variable 'instance' is a Reading");
        }


        /// <summary>
        /// Test the property 'Id'
        /// </summary>
        [Test]
        public void IdTest()
        {
            // TODO unit test for the property 'Id'
        }
        /// <summary>
        /// Test the property 'ProbeId'
        /// </summary>
        [Test]
        public void ProbeIdTest()
        {
            // TODO unit test for the property 'ProbeId'
        }
        /// <summary>
        /// Test the property 'ProbeName'
        /// </summary>
        [Test]
        public void ProbeNameTest()
        {
            // TODO unit test for the property 'ProbeName'
        }
        /// <summary>
        /// Test the property 'FieldId'
        /// </summary>
        [Test]
        public void FieldIdTest()
        {
            // TODO unit test for the property 'FieldId'
        }
        /// <summary>
        /// Test the property 'Timestamp'
        /// </summary>
        [Test]
        public void TimestampTest()
        {
            // TODO unit test for the property 'Timestamp'
        }
        /// <summary>
        /// Test the property 'Location'
        /// </summary>
        [Test]
        public void LocationTest()
        {
            // TODO unit test for the property 'Location'
        }
        /// <summary>
        /// Test the property 'Probe'
        /// </summary>
        [Test]
        public void ProbeTest()
        {
            // TODO unit test for the property 'Probe'
        }
        /// <summary>
        /// Test the property 'MicroClimate'
        /// </summary>
        [Test]
        public void MicroClimateTest()
        {
            // TODO unit test for the property 'MicroClimate'
        }
        /// <summary>
        /// Test the property 'MacroClimate'
        /// </summary>
        [Test]
        public void MacroClimateTest()
        {
            // TODO unit test for the property 'MacroClimate'
        }

    }

}
