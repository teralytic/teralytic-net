# IO.Teralytic.Api.OrganizationApi

All URIs are relative to *https://api.teralytic.io/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**OrganizationGet**](OrganizationApi.md#organizationget) | **GET** /organizations/{organization_id} | List single Organization details associated with Organization id provided
[**OrganizationList**](OrganizationApi.md#organizationlist) | **GET** /organizations | List all Organizations associated with account key
[**OrganizationReadingList**](OrganizationApi.md#organizationreadinglist) | **GET** /organizations/{organization_id}/readings | List all Readings associated with account key


<a name="organizationget"></a>
# **OrganizationGet**
> Organization OrganizationGet (Guid? organizationId)

List single Organization details associated with Organization id provided

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Teralytic.Api;
using IO.Teralytic.Client;
using IO.Teralytic.Model;

namespace Example
{
    public class OrganizationGetExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: OAuth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new OrganizationApi();
            var organizationId = new Guid?(); // Guid? | id of Organization to retrieve

            try
            {
                // List single Organization details associated with Organization id provided
                Organization result = apiInstance.OrganizationGet(organizationId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling OrganizationApi.OrganizationGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**Guid?**](Guid?.md)| id of Organization to retrieve | 

### Return type

[**Organization**](Organization.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="organizationlist"></a>
# **OrganizationList**
> List<Organization> OrganizationList ()

List all Organizations associated with account key

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Teralytic.Api;
using IO.Teralytic.Client;
using IO.Teralytic.Model;

namespace Example
{
    public class OrganizationListExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: OAuth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new OrganizationApi();

            try
            {
                // List all Organizations associated with account key
                List&lt;Organization&gt; result = apiInstance.OrganizationList();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling OrganizationApi.OrganizationList: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List<Organization>**](Organization.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="organizationreadinglist"></a>
# **OrganizationReadingList**
> List<Reading> OrganizationReadingList (Guid? organizationId, DateTime? startDate = null, DateTime? endDate = null, string rdate = null, long? limit = null, List<Guid?> fields = null, List<string> probes = null, string operation = null, string operationDepth = null)

List all Readings associated with account key

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Teralytic.Api;
using IO.Teralytic.Client;
using IO.Teralytic.Model;

namespace Example
{
    public class OrganizationReadingListExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: OAuth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new OrganizationApi();
            var organizationId = new Guid?(); // Guid? | id of Organization to retrieve
            var startDate = 2013-10-20T19:20:30+01:00;  // DateTime? | Start date and time for the query in RFC3339 format (optional) 
            var endDate = 2013-10-20T19:20:30+01:00;  // DateTime? | End date and time for the query in RFC3339 format (optional) 
            var rdate = rdate_example;  // string | Time period relative to start to return (optional)  (default to -24h)
            var limit = 789;  // long? | The maximum number of reading sets to return (grouped by reading id) (optional)  (default to 100)
            var fields = new List<Guid?>(); // List<Guid?> | The fields to return readings for (optional) 
            var probes = new List<string>(); // List<string> | The probes to return readings for (optional) 
            var operation = operation_example;  // string | Operation to perform on reading data (optional) 
            var operationDepth = operationDepth_example;  // string | The depth values to perform the operations function on (optional) 

            try
            {
                // List all Readings associated with account key
                List&lt;Reading&gt; result = apiInstance.OrganizationReadingList(organizationId, startDate, endDate, rdate, limit, fields, probes, operation, operationDepth);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling OrganizationApi.OrganizationReadingList: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**Guid?**](Guid?.md)| id of Organization to retrieve | 
 **startDate** | **DateTime?**| Start date and time for the query in RFC3339 format | [optional] 
 **endDate** | **DateTime?**| End date and time for the query in RFC3339 format | [optional] 
 **rdate** | **string**| Time period relative to start to return | [optional] [default to -24h]
 **limit** | **long?**| The maximum number of reading sets to return (grouped by reading id) | [optional] [default to 100]
 **fields** | [**List&lt;Guid?&gt;**](Guid?.md)| The fields to return readings for | [optional] 
 **probes** | [**List&lt;string&gt;**](string.md)| The probes to return readings for | [optional] 
 **operation** | **string**| Operation to perform on reading data | [optional] 
 **operationDepth** | **string**| The depth values to perform the operations function on | [optional] 

### Return type

[**List<Reading>**](Reading.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

