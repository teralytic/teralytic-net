# IO.Teralytic.Model.Reading
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | Base58 encoded identifier for this Reading. Corresponds with a single Probe and single timestamp. | 
**ProbeId** | **string** | id of probe from which Reading was made | 
**ProbeName** | **string** | Name of probe from which Reading was made | [optional] 
**FieldId** | **Guid?** | The field id of the probe | [optional] 
**Timestamp** | **string** | Time when Reading was measured | [optional] 
**Location** | [**ReadingLocation**](ReadingLocation.md) |  | [optional] 
**Probe** | [**List&lt;DepthReading&gt;**](DepthReading.md) | Probe readings | [optional] 
**MicroClimate** | [**MicroClimateData**](MicroClimateData.md) |  | [optional] 
**MacroClimate** | [**WeatherData**](WeatherData.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

