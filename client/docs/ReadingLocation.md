# IO.Teralytic.Model.ReadingLocation
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Latitude** | **decimal?** |  | [optional] 
**Longitude** | **decimal?** |  | [optional] 
**Geohash** | **string** | location geohash (12 digit precision) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

