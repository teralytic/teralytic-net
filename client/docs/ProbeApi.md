# IO.Teralytic.Api.ProbeApi

All URIs are relative to *https://api.teralytic.io/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ProbeGet**](ProbeApi.md#probeget) | **GET** /organizations/{organization_id}/probes/{probe_id} | List single Probe details, including Reading data, associated with Probe id provided (from set of Fields associated with the account key)
[**ProbeList**](ProbeApi.md#probelist) | **GET** /organizations/{organization_id}/probes | List all Probes associated with account key


<a name="probeget"></a>
# **ProbeGet**
> Probe ProbeGet (Guid? organizationId, string probeId)

List single Probe details, including Reading data, associated with Probe id provided (from set of Fields associated with the account key)

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Teralytic.Api;
using IO.Teralytic.Client;
using IO.Teralytic.Model;

namespace Example
{
    public class ProbeGetExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: OAuth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new ProbeApi();
            var organizationId = new Guid?(); // Guid? | id of Organization to retrieve
            var probeId = probeId_example;  // string | id of Probe to retrieve

            try
            {
                // List single Probe details, including Reading data, associated with Probe id provided (from set of Fields associated with the account key)
                Probe result = apiInstance.ProbeGet(organizationId, probeId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ProbeApi.ProbeGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**Guid?**](Guid?.md)| id of Organization to retrieve | 
 **probeId** | **string**| id of Probe to retrieve | 

### Return type

[**Probe**](Probe.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="probelist"></a>
# **ProbeList**
> List<Probe> ProbeList (Guid? organizationId, List<Guid?> fields = null)

List all Probes associated with account key

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Teralytic.Api;
using IO.Teralytic.Client;
using IO.Teralytic.Model;

namespace Example
{
    public class ProbeListExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: OAuth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new ProbeApi();
            var organizationId = new Guid?(); // Guid? | id of Organization to retrieve
            var fields = new List<Guid?>(); // List<Guid?> | Fields to filter search on (optional) 

            try
            {
                // List all Probes associated with account key
                List&lt;Probe&gt; result = apiInstance.ProbeList(organizationId, fields);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ProbeApi.ProbeList: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**Guid?**](Guid?.md)| id of Organization to retrieve | 
 **fields** | [**List&lt;Guid?&gt;**](Guid?.md)| Fields to filter search on | [optional] 

### Return type

[**List<Probe>**](Probe.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

