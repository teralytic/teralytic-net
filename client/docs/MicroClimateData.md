# IO.Teralytic.Model.MicroClimateData
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Humidity** | **decimal?** |  | [optional] 
**IrrigationSched** | **long?** | Irrigation Schedule | [optional] 
**IrrigationVolAcreIn** | **decimal?** | Irrigation volume in3/acre | [optional] 
**IrrigationVolAcreCm** | **decimal?** | Irrigation volume c3/acre | [optional] 
**Lux** | **decimal?** |  | [optional] 
**IrrigationGrossIn** | **decimal?** | Gross irrigation in in3 | [optional] 
**IrrigationNetIn** | **decimal?** | Net irrigation in in3 | [optional] 
**IrrigationGrossCm** | **decimal?** | Gross irrigation in cm3 | [optional] 
**IrrigationNetCm** | **decimal?** | Net irrigation in cm3 | [optional] 
**IrrigationRec** | **string** | Irrigation recommendation | [optional] 
**Temperature** | **decimal?** | Air Temperature | [optional] 
**Eto** | **decimal?** | Evaporative Transpiration | [optional] 
**IrrigationStream** | **decimal?** |  | [optional] 
**OrganicMatter** | **decimal?** | Organic Matter | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

