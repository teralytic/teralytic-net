# IO.Teralytic.Model.DepthReading
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Depth** | **long?** | The depth value | [optional] 
**DepthUnits** | **string** | The depth units (i.e. in, cm) | [optional] 
**Terascore** | **decimal?** | TeraScore | [optional] 
**Nitrogen** | **decimal?** | Nitrogen | [optional] 
**NitrogenPpm** | **decimal?** | Nitrogen ppm | [optional] 
**Phosphorus** | **decimal?** | Phosphorus | [optional] 
**PhosphorusPpm** | **decimal?** | Phosphorus ppm | [optional] 
**Potassium** | **decimal?** | Potassium | [optional] 
**PotassiumPpm** | **decimal?** | Potassium ppm | [optional] 
**Ec** | **decimal?** | Electrical Conductivity | [optional] 
**O2** | **decimal?** | Dioxygen | [optional] 
**PH** | **decimal?** | pH Scale | [optional] 
**Co2** | **decimal?** | Carbon Dioxide | [optional] 
**Awc** | **decimal?** |  | [optional] 
**SoilMoisture** | **decimal?** | Soil Moisture | [optional] 
**SoilTemperature** | **decimal?** | Soil Temperature | [optional] 
**SoilTexture** | **string** | Soil texture type. Data are collected from government data sources (USDA&#39;s SSURGO database for fields in the United States) | [optional] 
**ExtendedAttributes** | **Dictionary&lt;string, Object&gt;** | Additional properties | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

