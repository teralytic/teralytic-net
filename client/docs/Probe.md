# IO.Teralytic.Model.Probe
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid?** |  | [optional] 
**SerialCode** | **string** |  | [optional] 
**FieldId** | **Guid?** |  | [optional] 
**OrganizationId** | **Guid?** |  | [optional] 
**LastReading** | [**Reading**](Reading.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

