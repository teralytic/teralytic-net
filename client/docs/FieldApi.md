# IO.Teralytic.Api.FieldApi

All URIs are relative to *https://api.teralytic.io/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**FieldGet**](FieldApi.md#fieldget) | **GET** /organizations/{organization_id}/fields/{field_id} | List single Field details associated with Field id provided (from set of Fields associated with the account key)
[**FieldList**](FieldApi.md#fieldlist) | **GET** /organizations/{organization_id}/fields | List all Fields associated with account key


<a name="fieldget"></a>
# **FieldGet**
> Field FieldGet (Guid? organizationId, Guid? fieldId)

List single Field details associated with Field id provided (from set of Fields associated with the account key)

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Teralytic.Api;
using IO.Teralytic.Client;
using IO.Teralytic.Model;

namespace Example
{
    public class FieldGetExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: OAuth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new FieldApi();
            var organizationId = new Guid?(); // Guid? | id of Organization to retrieve
            var fieldId = new Guid?(); // Guid? | id of Field to retrieve

            try
            {
                // List single Field details associated with Field id provided (from set of Fields associated with the account key)
                Field result = apiInstance.FieldGet(organizationId, fieldId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FieldApi.FieldGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**Guid?**](Guid?.md)| id of Organization to retrieve | 
 **fieldId** | [**Guid?**](Guid?.md)| id of Field to retrieve | 

### Return type

[**Field**](Field.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="fieldlist"></a>
# **FieldList**
> List<Field> FieldList (Guid? organizationId, List<string> points = null)

List all Fields associated with account key

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Teralytic.Api;
using IO.Teralytic.Client;
using IO.Teralytic.Model;

namespace Example
{
    public class FieldListExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: OAuth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new FieldApi();
            var organizationId = new Guid?(); // Guid? | id of Organization to retrieve
            var points = new List<string>(); // List<string> | Array of points (lat,lng) describing a polygon search pattern. (optional) 

            try
            {
                // List all Fields associated with account key
                List&lt;Field&gt; result = apiInstance.FieldList(organizationId, points);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FieldApi.FieldList: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**Guid?**](Guid?.md)| id of Organization to retrieve | 
 **points** | [**List&lt;string&gt;**](string.md)| Array of points (lat,lng) describing a polygon search pattern. | [optional] 

### Return type

[**List<Field>**](Field.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

