# IO.Teralytic.Model.Field
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid?** |  | 
**OrganizationId** | **Guid?** |  | [optional] 
**Name** | **string** |  | 
**Acreage** | **decimal?** |  | [optional] 
**Crop** | **string** |  | [optional] 
**Geometry** | [**FieldGeometry**](FieldGeometry.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

