# IO.Teralytic.Model.WeatherData
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**WeatherSummary** | **string** | Weather Summary | [optional] 
**PrecipType** | **string** | Precipitation Type | [optional] 
**ApparentTemp** | **decimal?** | Apparent Temperature | [optional] 
**CloudCover** | **decimal?** | Cloud Cover | [optional] 
**DewPoint** | **decimal?** | Dew Point | [optional] 
**Humidity** | **decimal?** | Humidity | [optional] 
**Ozone** | **decimal?** | Ozone Rating | [optional] 
**PrecipIntensity** | **decimal?** | Precipitation Intesity | [optional] 
**PrecipProbability** | **decimal?** | Precipitation Probability | [optional] 
**Pressure** | **decimal?** | Barometric Pressure | [optional] 
**Temperature** | **decimal?** | Temperature | [optional] 
**Time** | **decimal?** | Weather Sample Time (epoch) | [optional] 
**UvIndex** | **int?** | UV Index | [optional] 
**WindBearing** | **int?** | Wind Bearing | [optional] 
**WindGust** | **decimal?** | Wind Gust | [optional] 
**WindSpeed** | **decimal?** | Wind Speed | [optional] 
**Visibility** | **decimal?** | Visibility | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

